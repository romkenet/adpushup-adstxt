This library provides adpushup API for handling ads.txt management.

# Django:

## Add it to installed apps:

```python
INSTALLED_APPS = (
    ...,
    'adpushup_adstxt',
    ...,
)
```

## Add your user_id and key to YOUR `settings.py`:

```python
ADPUSHUP_API_USER_ID = 'test@example.com'
ADPUSHUP_API_KEY = '1234'
```

## OPTIONALLY add different `WWW_DIR`:

```python
ADPUSHUP_WWW_DIR = '/some/dir/to/put/ads.txt/in/it/'
```

   by default it is Djangos `ROOT_DIR + '/www'`

## Add it to your `urls.py`:

```python
from adpushup_adstxt.django_views import handle

urlpatterns += patterns(
    '',
    url(r'^adsTxtManagementApiByAdpushup.php', handle),
)
```

# Testing:

```python
from adpushup_adstxt.utils import encode_uri_component
import time
import hmac
import hashlib
import requests

user_id = 'your user id'
key = 'your key'

req_time = int(time.time())
hash_params = "email={}&ts={}".format(
    encode_uri_component(user_id.encode("UTF-8")), req_time)
hash = hmac.new(key, hash_params, hashlib.sha256).hexdigest()

res = requests.post(
    'http://localhost:8000/adsTxtManagementApiByAdpushup.php',
    dict(
        data='test content',
        ts=req_time,
        hash=hash))

print res.status_code
print res.content
```